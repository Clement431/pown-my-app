# Installation

- Basculer sur le branche vuln
- Il faut créer une BDD mysql grâce au fichier vuln.sql
- Dans le code, il faut changer les informations pour accéder à la BDD dans configdb.php


# Lancement de l'application
- Faire un `php -S localhost:8000` pour lancer le serveur et aller sur la page http://localhost:8000/login.php
- Pour se connecter, il faut utiliser : identifiant Clement, mot de passe azerty


# Correction des failles
- Pour corriger les failles XSS, on peut utiliser la méthode `htmlspecialchars` qui permet de convertir tous les caractères spéciaux HTML dans leurs encodages HTML.

- Pour éviter les injections sql il est préférable d'utiliser des requêtes `prepare ` exemple :
```
$st = $db->prepare("SELECT `id`, `username`, `password` FROM `user` WHERE `username` =:user");
$st->execute(array(
    ':user' => $_POST["user"],
));
$user = $st->fetchAll();
```

- Pour se protéger des attaque CSRF j'ai essayé de mettre en place un token CSRF. J'ai donc généré un token qui va être placé dans le formulaire et aussi stocker dans la BDD. Après que le formulaire soit submit on vérifie que le token est le même coté back que coté front